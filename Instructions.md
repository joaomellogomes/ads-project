Oi pessoal, atendendo às reivindicações de vocês, refiz o plano de avaliação: AVALIAÇÃO da disciplina 
ANÁLISE E DESENVOLVIMENTO DE SISTEMAS 

#OPÇÃO 1

  Pesquise sobre o tema bigdata/data-mining e escreva um relatório 
  descrevendo quais suas características e aplicaçoes. Em seguida liste algumas ferramentas gratuitas que existem 
  listando suas principais características. Se for possível, instale alguma ferramenta em seu computador para testar
  e diga com suas palavras quais as suas impressões. O relatório não deve tomar mais do que quatro ou cinco páginas,
  incluindo capa, texto, figuras, tabelas, referências. Importante: não copie textos e faça as referências corretamente
  seguindo normas da ABNT. https://tecnoblog.net/236041/guia-normas-abnt-trabalho-academico-tcc/
  
#OPÇÃO 2:
  >> PROJETO: Montagem de um banco de dados com base inspirada em uma situação real <<
  O sistema pode servir para controlar:  
  
    -    Utensílios em uma loja. -  Controle de itens de uma coleção de objetos (moedas, livros, CDs).
    
    -    Estoque de mantimentos em uma casa ou roupas no armário. 
    
    -    Medicamentos no armário de uma residência ou mesmo uma clínic
    
    -    Outra situação que reporte a algo real. 
  
  * Parte 1: 

    Requisitos (3,0 pontos) Documento deve descrever na forma de uma lista quais os requisitos
    funcionais e não funcionais, conforme AULA 4 e a entrega de um documento de requisitos simplificado
    conforme AULA 5. A especificação pode ser em linguagem natural ou não, conforme o desenvolvimento do
    projeto Enviar o LINK do documento de requisitos.
  
  * Parte 2:

    Implementação (5,0 pontos) Leia a AULA 6 e
    adote uma ferramenta que achar mais conveniente para a implementação.  Enviar o LINK do sistema.
    
  * Parte 3:

    Documentação para o usuário (2,0 pontos) De forma simplificada, apresente ao usuário como dele deve usar
    o sistema, apresentado todas as principais funcionalidades.  Enviar o LINK do manual ou guia do usuário.
    Observação: trabalho pode ser em dupla desde que claramente especificado quem fez o quê mas a preferência
    é pelo desenvolvimento individual . Serão aceitos apenas links, não remeter arquivos. Incluir o nome de
    todas as pessoas em todos os links. 
    
Data de entrega: 1 de junho - Bom trabalho! Este e-mail foi enviado
automaticamente. Favor não responder.