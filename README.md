# PROJETO ARMAZÉM

O projeto tem como intuito o desenvolvimento de um banco de dados relacional, o qual deve atender aos **requisitos** de um armazém. Neste armazém é dada a falta de controle sobre as vendas que os vendedores vendem, as peças que são compradas, e quais vendedores e peças os clientes são atendidos e compram, sendo assim temos as seguintes entidades:

* Armazeḿ
* Vendedor
* Cliente
* Peça
* Pedido

Sendo assim para a elaboração do banco de dados relacional que atenda as necessidades desse negócio é preciso entender os requisitos (funcionais e não funcionais).

## REQUISITOS

### Funcionais:
Os requisitos funcionais são as ações que são necessárias serem executadas dentro do sistema para um produto mínimo viável (MVP). Sendo assim podemos considerar os seguintes items para o negócio:

- Armazenar o vendedor que o cliente foi atendido
- Armazenar os pedidos do cliente
- Armazenar as peças que o cliente incluiu no pedido
- Armazenar o armazém que o vendedor trabalha
- Armazenar as peças no estoque

### Não Funcionais:
Os requisitos não funcionais são aqueles que não impactam no resultado final do que é entregue no MVP para o usuário. Podemos entender como as ferramentas e o modo que alcançaremos o objetivo final, como quais metodologias, paradigmas, e tecnologias utilizaremos:

- Banco de dados relacional (MySQL)
- MySQL Workbench para edição do script de bancos de dados
- Sistema operacional linux para rodar a primeira versão do banco