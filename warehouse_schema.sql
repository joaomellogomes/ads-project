DROP DATABASE IF EXISTS warehouse_schema;

CREATE DATABASE IF NOT EXISTS warehouse_schema;
USE warehouse_schema;

create table warehouse(
  cod_warehouse int(2) primary key not null auto_increment,
  warehouse_name varchar(75),
  warehouse_address varchar(150)
);

create table seller(
  cod_seller int(4) primary key auto_increment not null,
  seller_name varchar(150),
  seller_address varchar(150)
);

create table customer(
  cod_customer int(4) primary key auto_increment not null,
  customer_name varchar(150),
  customer_address varchar(150),
  billing decimal(7,2),
  credit_limit decimal(7,2),

  cod_seller int(4),
  CONSTRAINT fk_customer_cod_seller FOREIGN KEY (cod_seller) REFERENCES seller(cod_seller)
  ON UPDATE CASCADE
);

create table piece(
  cod_piece int(6) primary key not null auto_increment,
  piece_description varchar(200)
);

create table order_schema(
  cod_order int(5) primary key auto_increment not null,
  num_order int,
  order_date datetime,
  order_address varchar(150),
  comission decimal(7,2),

  cod_customer int(4),
  CONSTRAINT fk_order_schema_cod_customer FOREIGN KEY (cod_customer) REFERENCES customer(cod_customer)
  ON UPDATE CASCADE,

  cod_seller int(4),
  CONSTRAINT fk_order_schema_codigo_vendedor FOREIGN KEY (cod_seller) REFERENCES seller(cod_seller)
  ON UPDATE CASCADE
);

create table piece_warehouse(
  inventory_qty int(3),
  price decimal(7,2),

  cod_piece int(6),
  CONSTRAINT fk_piece_warehouse_cod_piece FOREIGN KEY (cod_piece) REFERENCES piece(cod_piece)
  ON UPDATE CASCADE,

  cod_warehouse int(2),
  CONSTRAINT fk_piece_warehouse_cod_warehouse FOREIGN KEY (cod_warehouse) REFERENCES warehouse(cod_warehouse)
  ON UPDATE CASCADE
);

create table seller_warehouse(
  cod_seller int(4),
  cod_warehouse int(2),

  CONSTRAINT fk_seller_warehouse_cod_seller FOREIGN KEY (cod_seller) REFERENCES seller(cod_seller)
  CONSTRAINT fk_seller_warehouse_cod_warehouse FOREIGN KEY (cod_warehouse) REFERENCES warehouse(cod_warehouse)
);

create table order_piece(
  quoted_price decimal(7,2),
  cod_order int(5),
  cod_piece int(6),

  CONSTRAINT fk_order_piece_cod_order FOREIGN KEY (cod_order) REFERENCES order_schema(cod_order)
  CONSTRAINT fk_order_piece_cod_piece FOREIGN KEY (cod_piece) REFERENCES piece(cod_piece)
);