### DOCUMENTAÇÃO

Este projeto inclui um script de banco de dados pronto para ser utilizado. A linguagem utilizada foi SQL utilizando MySQL. Portanto para execução do script é necessário que instale o MySQL na sua máquina:

https://dev.mysql.com/doc/refman/8.0/en/installing.html

Após a instalação, importe o script e já é possível testar o funcionamento do banco de dados.

Utilizando este modelo é possível:

- Armazenar o vendedor que o cliente foi atendido
- Armazenar os pedidos do cliente
- Armazenar as peças que o cliente incluiu no pedido
- Armazenar o armazém que o vendedor trabalha
- Armazenar as peças no estoque